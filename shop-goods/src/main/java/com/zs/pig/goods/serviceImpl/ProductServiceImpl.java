/** Powered By zscat科技, Since 2016 - 2020 */

package com.zs.pig.goods.serviceImpl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zs.pig.common.base.ServiceMybatis;
import com.zs.pig.goods.api.model.Product;
import com.zs.pig.goods.api.service.ProductService;
import com.zs.pig.goods.mapper.ProductMapper;
/**
 * 
 * @author zsCat 2016-12-22 9:29:05
 * @Email: [email]
 * @version [version]
 *	项目管理
 */
@Service("ProductService")
public class ProductServiceImpl  extends ServiceMybatis<Product> implements ProductService {

	@Resource
	private ProductMapper ProductMapper;

	
	/**
	 * 保存或更新
	 * 
	 * @param Product
	 * @return
	 */
	public int saveProduct(Product Product) {
		return this.save(Product);
	}

	/**
	 * 删除
	* @param Product
	* @return
	 */
	public int deleteProduct(Product Product) {
		return this.delete(Product);
	}

   @Override
	public PageInfo<Product> findPageInfo(Map<String, Object> params) {
		PageHelper.startPage(params);
		List<Product> list = ProductMapper.findPageInfo(params);
		return new PageInfo<Product>(list);
	}

@Override
public List<Product> selectProductByFloor(Long id) {
	return ProductMapper.selectProductByFloor(id);
}

@Override
public List<Product> getProductByFloorid(Long tid) {
	// TODO Auto-generated method stub
	return ProductMapper.getProductByFloorid(tid);
}

@Override
public PageInfo<Product> selectgoodsListByType(int i, int j, Product g) {
	PageHelper.startPage(i, j);
	List<Product> list = ProductMapper.selectgoodsListByType(g);
	return new PageInfo<Product>(list);
}

@Override
public List<Product> selectRepoer() {
	// TODO Auto-generated method stub
	return ProductMapper.selectRepoer();
}
}
