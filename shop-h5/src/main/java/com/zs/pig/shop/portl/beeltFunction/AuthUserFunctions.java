package com.zs.pig.shop.portl.beeltFunction;

import org.springframework.stereotype.Component;

import com.zs.pig.shop.portl.util.MemberUtils;
import com.zs.pig.user.api.model.Member;

@Component
public class AuthUserFunctions {

	public Member getLoginUser(){
		return MemberUtils.getSessionLoginUser();
	}
	public boolean isLogin(){
		return MemberUtils.getSessionLoginUser()!=null;
	}
}
