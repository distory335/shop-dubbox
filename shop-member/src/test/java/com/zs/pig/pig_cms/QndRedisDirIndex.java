package com.zs.pig.pig_cms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;

import ch.qos.logback.classic.Level;

import com.github.abel533.database.Dialect;
import com.github.abel533.database.SimpleDataSource;
import com.github.abel533.utils.DBMetadataUtils;
import com.github.ddth.com.redir.RedisDirectory;

public class QndRedisDirIndex extends BaseQndRedisDir {
	public static String dbUrl="jdbc:mysql://localhost:3306/pig?useUnicode=true&characterEncoding=utf8";
	public static String dbName="pig";
	
	public static  String dbUserName="root";
	public static  String dbPassword="root";
	public static  String jdbcName="com.mysql.jdbc.Driver";
    public static void main(String args[]) throws Exception {
        initLoggers(Level.INFO);
        RedisDirectory DIR = new RedisDirectory(REDIS_HOST, REDIS_PORT, REDIS_PASSWORD);
        DIR.init();

        long t1 = System.currentTimeMillis();
        try {
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);

//            IndexWriter iw = new IndexWriter(DIR, iwc);
//            Document doc = new Document();
//            doc.add(new StringField("id", "thanhnb", Field.Store.YES));
//            doc.add(new TextField("name", "Nguyen Ba Thanh", Field.Store.NO));
//            iw.updateDocument(new Term("id", "thanhnb"), doc);
            DBMetadataUtils dbUtils = new DBMetadataUtils(
		            new SimpleDataSource(Dialect.MYSQL, dbUrl, dbUserName, dbPassword));
            IndexWriter iw = new IndexWriter(DIR, iwc);
		//    List<IntrospectedTable> tables = dbUtils.introspectTables(dbUtils.getDefaultConfig());
		 Connection con=dbUtils.getConnection();
		 String sql = "select * from t_blog";
		    PreparedStatement pstmt;
		    try {
		        pstmt = (PreparedStatement)con.prepareStatement(sql);
		        ResultSet rs = pstmt.executeQuery();
		       
		        while (rs.next()) {
		        	 Document doc = new Document();
		        	System.out.println(rs.getString("title"));
		        	doc.add(new TextField("id", rs.getLong("id")+"", Store.YES));
					doc.add(new TextField("title", rs.getString("title"), Store.YES));
					doc.add(new TextField("summary", rs.getString("summary"), Store.YES));
					doc.add(new TextField("releaseDate", rs.getString("releaseDate"), Store.YES));
					doc.add(new TextField("content", rs.getString("content"), Store.YES));
					doc.add(new TextField("typeId", rs.getLong("typeId")+"", Store.YES));
					iw.addDocument(doc);
					iw.commit();
		        }
		            System.out.println("============================");
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
		    
           
           

            iw.close();
        } finally {
            DIR.destroy();
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Finished in " + (t2 - t1) / 1000.0 + " sec");
    }

}
