/** Powered By zscat科技, Since 2016 - 2020 */

package com.zs.pig.goods.api.service;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.zs.pig.common.base.BaseService;
import com.zs.pig.goods.api.model.ProductType;

 /**
 * 
 * @author zsCat 2016-12-22 9:29:55
 * @Email: [email]
 * @version [version]
 *	项目类别管理
 */
public interface ProductTypeService extends BaseService<ProductType>{

	

	
	/**
	 * 保存或更新
	 * 
	 * @param ProductType
	 * @return
	 */
	public int saveProductType(ProductType ProductType) ;
	/**
	 * 删除
	* @param ProductType
	* @return
	 */
	public int deleteProductType(ProductType ProductType);

	public PageInfo<ProductType> findPageInfo(Map<String, Object> params);

}
